<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:2|max:255',
            'title_fr' => 'required|min:2|max:255',
            'brand' => 'required|min:2|max:255',
            'condition' => 'required|in:new',
            'status' => 'required|in:active,close,new,deleted',
            'quantity_available' => 'required|numeric',
            'quantity_sold' => 'required|numeric',
            'sku' => 'required|numeric',
        ];
    }

       /**
     * Get error response in case validation failed.
     *
     * @param array $errors
     * @return json
     */
    public function response(array $errors)
    {
        return response()->json([
            'errors' => $errors
        ]);
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Title is required!',
            'title_fr.required' => 'Title French is required!',
            'status.in' => 'Available status options: active, close, new, deleted!',
            'condition.in' => 'Available status options: new!',
        ];
    }
}
