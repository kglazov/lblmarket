<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $postRepository;

    /**
     * Create a new controller instance.
     *
     * @param  PostRepository  $productRepository
     * @return void
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return new PostCollection($this->postRepository->all());
    }

    public function show($id)
    {
        return new PostResource($this->postRepository->getPost($id));
    }

    /**
     * Store a new post.
     *
     * @param  StorePostRequest  $request
     * @return PostResource
     */
    public function store(StorePostRequest $request)
    {
        $post = Post::create($request->all());

        return (new PostResource($post))
            ->response()
            ->setStatusCode(201);
    }

    public function delete($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        return response()->json(null, 204);
    }
}
