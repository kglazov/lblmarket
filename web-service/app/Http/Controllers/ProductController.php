<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\Product as ProductResource;
use App\Repositories\ProductRepository;

use App\Models\Product;

class ProductController extends Controller
{
    private $productRepository;

    /**
     * Create a new controller instance.
     *
     * ProductController constructor.
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return ProductCollection
     */
    public function index()
    {
        return new ProductCollection($this->productRepository->all());
    }

    /**
     * @param $id
     * @return ProductResource
     */
    public function show($id)
    {
        return new ProductResource($this->productRepository->getProduct($id));
    }

    /**
     * Store a new product.
     *
     * @param StoreProductRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreProductRequest $request)
    {
        $product = Product::create($request->all());

        return (new ProductResource($product))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreProductRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(StoreProductRequest $request, $id)
    {
        $product = $this->productRepository->getProduct($id);
        $product->update($request->all());

        return (new ProductResource($product))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Delete product
     *
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $product = $this->productRepository->getProduct($id);
        $product->delete();
        return response()->json(null, 204);
    }
}
