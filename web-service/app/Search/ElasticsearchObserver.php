<?php

namespace App\Search;

use App\Models\Product;
use Elasticsearch\Client;

/**
 * Hook into the Eloquent models we want to index on it and send some HTTP requests to the Elasticsearch API
 */
class ElasticsearchObserver
{
    /** @var \Elasticsearch\Client */
    private $elasticsearch;

    public function __construct(Client $elasticsearch)
    {
        $this->elasticsearch = $elasticsearch;
    }

    public function saved($model)
    {
        $this->elasticsearch->index([
            'index' => $model->getSearchIndex(),
            'id' => $model->getKey(),
            'title' => $model->getSearchTitle(), 
            'title_fr' => $model->getSearchTitleFr(), 
            'brand' => $model->getSearchBrand(),
            'condition' => $model->getSearchCondition(), 
            'status' => $model->getSearchStatus(), 
            'quantity_available' => $model->getSearchQuantityAvailable(),
            'quantity_sold' => $model->getSearchQuantitySold(), 
            'sku' => $model->getSearchSku(), 
        ]);
    }

    public function deleted($model)
    {
        $this->elasticsearch->delete([
            'index' => $model->getSearchIndex(),
            'id' => $model->getKey(),
            'title' => $model->getSearchTitle(), 
            'brand' => $model->getSearchBrand(),
        ]);
    }
}