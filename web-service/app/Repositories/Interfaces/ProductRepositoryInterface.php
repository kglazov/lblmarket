<?php

namespace App\Repositories\Interfaces;

use App\User;
use Illuminate\Database\Eloquent\Collection;

interface ProductRepositoryInterface
{
    public function all();

    public function getByUser(User $user);
    
    public function search(string $query = ''): Collection;
}
