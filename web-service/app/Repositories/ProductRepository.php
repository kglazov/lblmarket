<?php

namespace App\Repositories;

use App\Models\Product;
use App\User;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ProductRepository implements ProductRepositoryInterface
{
    public function all()
    {
        return Product::all();
        // return Blog::orderBy('id', 'desc')->paginate(5);
    }

    public function getProduct($id) 
    {
        return Product::findOrFail($id);
    }

    public function getByUser(User $user)
    {
        
    }

    public function search(string $query = ''): Collection
    {
        return Product::query()
            ->where('brand', 'like', "%{$query}%")
            ->orWhere('title', 'like', "%{$query}%")
            ->get();
    }
}
