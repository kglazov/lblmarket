<?php

namespace App\Repositories;

use App\User;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;

class EloquentRepository implements ProductRepositoryInterface
{
    public function all()
    {
        return Product::all();
        // return Blog::orderBy('id', 'desc')->paginate(5);
    }

    public function getProduct($id) 
    {
        return Product::findOrFail($id);
    }

    public function getByUser(User $user)
    {
        
    }

    public function search(string $query = ''): Collection
    {
        return Product::query()
            ->where('brand', 'like', "%{$query}%")
            ->orWhere('title', 'like', "%{$query}%")
            ->get();
    }
}
