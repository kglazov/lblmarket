<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use App\Articles\ArticlesRepository;

Route::get('/search', function (ArticlesRepository $repository) {
    $articles = $repositry->search((string) request('q'));

    return view('articles.index', [
        'articles' => $articles,
    ]);
});

Route::get('/', function () {
    return view('welcome');
});

