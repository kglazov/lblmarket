<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Using implicit route model binding.
 * This way, Laravel will inject the Product instance in our methods and automatically return a 404 if it isn’t found.
 */
Route::get('/product', 'ProductController@index');
Route::get('/product/{id}', 'ProductController@show');
// Route::get('/product/{product}', 'ProductController@show');
Route::post('/product', 'ProductController@store');
Route::put('/product/{id}', 'ProductController@update');
// Route::put('/product/{product}', 'ProductController@update');
Route::delete('/product/{id}', 'ProductController@delete');
// Route::delete('/product/{product}', 'ProductController@delete');


// Route::get('/search', function (ProductRepository $repository) {
//     $products = $repositry->search((string) request('q'));

//     return view('articles.index', [
//         'articles' => $articles,
//     ]);
// });