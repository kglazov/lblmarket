<?php

namespace Tests\Feature;


use App\Models\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ProductTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseMigrations;

    public function testsProductsAreCreatedCorrectly()
    {
        $payload = [
            'title' => 'Yogurt',
            'title_fr' => 'Yogurt French',
            'brand' => 'Activia',
            'condition' => 'new',
            'status' => 'new',
            'quantity_available' => 400,
            'quantity_sold' => 40,
            'sku' => '098098098098',
        ];

        $response = $this->post('/api/product', $payload);

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertDatabaseHas('products', $payload);
    }

    public function testsProductsAreUpdatedCorrectly()
    {
        $product = factory(Product::class)->create();
        
        $payload = [
            'title' => 'Lorem',
            'title_fr' => $product->title_fr,
            'brand' => 'Ipsum',
            'condition' => $product->condition,
            'status' => $product->status,
            'quantity_available' => $product->quantity_available,
            'quantity_sold' => $product->quantity_available,
            'sku' => $product->sku,
        ];

        $response = $this->put('/api/product/' . $product->id, $payload);
        
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('products', $payload);
    }

    public function testsProductsAreDeletedCorrectly()
    {
        $product = factory(Product::class)->create();

        $response = $this->delete('/api/product/' . $product->id);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertDatabaseMissing('products', ['id' => $product->id, 'deleted_at' => null]);
    }

    public function testProductsAreFoundCorrectly()
    {
        $product = factory(Product::class)->create();

        $response = $this->get('/api/product/' . $product->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('products', ['id' => $product->id]);
    }

    public function testProductsAreListedCorrectly()
    {
        $products = factory(Product::class, 3)->create();

        $response = $this->get('/api/product');
        
        $data = $response->json();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(3, $data['data']);
    }
}
