<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('store_id')->default(1);
            $table->string('title');
            $table->string('title_fr');
            $table->string('brand');
            $table->enum('condition', ['new']);
            $table->enum('status', ['activate', 'closed', 'new', 'deleted']);
            $table->integer('quantity_available');
            $table->integer('quantity_sold');
            $table->string('sku');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
