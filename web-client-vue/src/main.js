import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.config.productionTip = false
Vue.use(VueRouter)

import Dashboard from './components/Dashboard.vue'
import Auth from '@okta/okta-vue'
import vuetify from './plugins/vuetify';
import Product from "./components/Product";


Vue.use(Auth, {
  issuer: 'https://dev-591630.okta.com/oauth2/default',
  client_id: '0oax4utwkkgyBxHDo356',
  redirect_uri: 'http://localhost:3000/implicit/callback',
  scope: 'openid profile email'
})

const routes = [
  { path: '/implicit/callback', component: Auth.handleCallback() },
  { path: '/product', component: Product }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  router,
  vuetify,
  render: h => h(Dashboard)
}).$mount('#app')
